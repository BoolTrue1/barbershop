// var doc = document.querySelector(".login-link");
// var log = document.querySelector(".modal-login");
// var test = log.querySelector(".modal-close");
// var test2 = log.querySelector(".input");

// doc.addEventListener("click", function(test3) {
//   test3.preventDefault();
//   console.log("ыыыыыыыы!");
//   log.classList.add("modal-show");
//   test2.focus()
// });

// test.addEventListener("click", function(test3) {
//   test3.preventDefault();
//   console.log("ы!");
//   log.classList.remove("modal-show");
// });

// function randomNumber() {
//     console.log(Math.random())
// }

// setInterval(randomNumber, 200)

const mainNavElement = document.querySelector('.main-nav');
const navWrapperElement = document.querySelector('.main-nav__wrapper');
const toggleNavElement = document.querySelector('.main-nav__toggle');
const mainNavIcon = document.querySelector('.main-nav__icon');

const reviewsItems = document.querySelectorAll('blockquote.reviews__item');
const sliderToggles = document.querySelectorAll('.reviews__toggles .slider__toggle');

toggleNavElement.addEventListener('click', function() {
    navWrapperElement.classList.toggle('main-nav__wrapper--hide');
    mainNavElement.classList.toggle('main-nav--toggle');
    changeNavIcon()
})

function changeNavIcon() {
    if (mainNavIcon.hasAttribute('width')) {
        mainNavIcon.setAttribute('src', 'img/nav-open-icon.svg')
        mainNavIcon.removeAttribute('width')
    } else {
        mainNavIcon.setAttribute('src', 'img/nav-close-icon.svg')
        mainNavIcon.setAttribute('width', '25px')
    }
}

function sliding() {

    const slide = document.querySelector('.reviews__item')
    const slideWidth = slide.getBoundingClientRect().width
    const buttons = document.querySelectorAll('.reviews__toggles .slider__toggle')
    const slider = document.querySelector('.reviews__wrapwrapper')
    

    buttons.forEach((button, index) => {
        button.addEventListener('click', event => {
            const scrollWidth = (index) * slideWidth;
            console.log(index);
            slider.style.transform = `translate(${-scrollWidth}px)`;
            for (let i; i > buttons.length; i++){
                buttons[i].classList.remove('slider__toggle--current');
            }
            button.classList.add('slider__toggle--current');
        
        })
    })

}sliding();





